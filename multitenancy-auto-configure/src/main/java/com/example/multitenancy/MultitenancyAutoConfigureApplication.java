package com.example.multitenancy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MultitenancyAutoConfigureApplication {

	public static void main(String[] args) {
		SpringApplication.run(MultitenancyAutoConfigureApplication.class, args);
	}
}
