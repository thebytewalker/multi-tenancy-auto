package com.example.multitenancy.autoconfig;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.commons.dbcp2.BasicDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
//import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.example.multitenancy.autoconfig.MulitenantDataSourceProperties.DataSourceProperties;

@Configuration
@AutoConfigureBefore(value= {DataSourceAutoConfiguration.class})
//@ConditionalOnProperty()
@EnableConfigurationProperties({MulitenantDataSourceProperties.class})
public class MultitenancyAutoConfig {
	
	@Autowired
	MulitenantDataSourceProperties dataSourceProperties;
	
	@SuppressWarnings("unchecked")
	protected <T> T createDataSource(DataSourceProperties properties,
			Class<? extends DataSource> type) {
		return (T) properties.initializeDataSourceBuilder().type(type).build();
	}
	
	@Bean(name = "datasource")
	@ConditionalOnMissingBean
	public DataSource dataSource() {
		TenantRoutingDataSource routingDataSource = new TenantRoutingDataSource();
		Map<Object, Object> targetDataSources = new HashMap<>();
		List<DataSourceProperties> tenantDataSources = dataSourceProperties.getDataSources();
		for(DataSourceProperties properties : tenantDataSources) {
			targetDataSources.put(properties.getTenantId(), createDataSource(properties, BasicDataSource.class));
			System.out.println(properties.getTenantId());
		}
		routingDataSource.setTargetDataSources(targetDataSources);
		routingDataSource.setDefaultTargetDataSource(targetDataSources.get("tenant1"));
		return routingDataSource;
	}

}
