package com.example.multitenancy.autoconfig;

import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;


@ConfigurationProperties(prefix = "msp.multitenancy")
public class MulitenantDataSourceProperties {

	private List<DataSourceProperties> dataSources;

	public List<DataSourceProperties> getDataSources() {
		return this.dataSources;
	}

	public void setDataSources(List<DataSourceProperties> dataSourcesProps) {
		this.dataSources = dataSourcesProps;
	}

	public static class DataSourceProperties extends org.springframework.boot.autoconfigure.jdbc.DataSourceProperties {

		private String tenantId;

		public String getTenantId() {
			return tenantId;
		}

		public void setTenantId(String tenantId) {
			this.tenantId = tenantId;
		}
	}
	
	@PostConstruct
	public void init() {
		System.out.println("constructed:===== "+this.dataSources);
	}
}

