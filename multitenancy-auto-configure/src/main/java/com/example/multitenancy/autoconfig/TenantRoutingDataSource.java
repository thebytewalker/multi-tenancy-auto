package com.example.multitenancy.autoconfig;

import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource;

public class TenantRoutingDataSource extends AbstractRoutingDataSource{

	@Override
	protected Object determineCurrentLookupKey() {
		return TenantResolver.getTenantIdenetifier();
	}

}
