package com.example.multitenancy.autoconfig;

public class TenantResolver {
	
	public static final ThreadLocal<String> TENANT_IDENTIFIER = new ThreadLocal<>();

	public static String getTenantIdenetifier() {
		return TENANT_IDENTIFIER.get();
	}

	public static void setTenantIdentifier(String tenantIdenetifier) {
		TENANT_IDENTIFIER.set(tenantIdenetifier);
	}

	
	

}
