package org.glymss.api;

import java.util.ArrayList;
import java.util.List;

import org.glymss.data.Student;
import org.glymss.data.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.multitenancy.autoconfig.TenantResolver;


@RestController
@RequestMapping("/student")
public class StudentController {

    @Autowired
    private StudentRepository repo;

    @GetMapping
    public List<Student> getStudents() {
        List<Student> students = new ArrayList<>();
        repo.findAll()
                .forEach(students::add);

        return students;
    }

    @PostMapping
    public void saveStudent(@RequestBody Student student) {
        repo.save(student);
    }

    @DeleteMapping(path = "/{id}")
    public void deleteStudent(@PathVariable Long id) {
        repo.delete(id);
    }

}
