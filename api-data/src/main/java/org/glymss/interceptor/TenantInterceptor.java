package org.glymss.interceptor;

import com.example.multitenancy.autoconfig.TenantResolver;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Component
public class TenantInterceptor implements HandlerInterceptor {

    private static final String X_TENANT_ID = "X-tenant-id";

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object o) {
        String tenantId = request.getHeader(X_TENANT_ID);
        System.out.println("Tenant Interceptor " + tenantId);
        if (tenantId != null && tenantId.isEmpty())
            return false;
        else
            TenantResolver.setTenantIdentifier(tenantId);
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, ModelAndView modelAndView) {

    }

    @Override
    public void afterCompletion(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, Exception e) {

    }
}
